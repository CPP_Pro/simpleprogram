// mycode.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <Windows.h>
#include <iostream>
#include <string>

using namespace std;

// function for ex 2.13
void rect(int& ar, int& per, int len, int width)
{
	ar = len * width;
	per = (len + width) * 2;
}

// function for ex 2.16
int* rotation(int* aa, int len)
{
	int* bb = new int[len];
	for (int i = 0; i < len - 1; i++)
	{
		bb[i] = aa[i + 1];
	}
	bb[len-1] = aa[0];
	return bb;
}

// function for ex 2.17
int maximum(int list[], int n)
{
	int max_val = list[0];
	for (int i = 0; i < n; i++)
	{
		if (max_val < list[i]) max_val = list[i];
	}
	return max_val;
}

// main function
int _tmain(int argc, _TCHAR* argv[])
{
	// input, output for ex 2.13
	cout << "Ex 2.13\n";
	int length, width;
	int area, perimeter;
	cout << "Input length and width\n";
	cout << "length:\n";
	cin >> length;
	cout << "width:\n";
	cin >> width;

	rect(area, perimeter, length, width);
	cout << "Area: " << area << " Perimeter: " << perimeter << " length: " << length << " width: " << width << "\n\n";

	// input,output for ex 2.16
	cout << "Ex 2.16\n";
	cout << "Input five numbers\n";
	int *aa = new int[5];
	int *bb = new int[5];
	cout << "aa[0]:\n";
	cin >> aa[0];
	cout << "aa[1]:\n";
	cin >> aa[1];
	cout << "aa[2]:\n";
	cin >> aa[2];
	cout << "aa[3]:\n";
	cin >> aa[3];
	cout << "aa[4]:\n";
	cin >> aa[4];

	bb = rotation(aa, 5);
	cout << "Rotation result:\n";
	cout << "bb[0]: " << bb[0] << " bb[1]: " << bb[1] << " bb[2]: " << bb[2] << " bb[3]: " << bb[3] << " bb[4]: " << bb[4] << "\n\n";

	// input,output for ex 2.17
	cout << "Ex 2.17\n";
	cout << "Input five numbers\n";
	cout << "aa[0]:\n";
	cin >> aa[0];
	cout << "aa[1]:\n";
	cin >> aa[1];
	cout << "aa[2]:\n";
	cin >> aa[2];
	cout << "aa[3]:\n";
	cin >> aa[3];
	cout << "aa[4]:\n";
	cin >> aa[4];

	int max_val = maximum(aa, 5);
	cout << "Maximum value: " << max_val << "\n\n";
	cout << "That's all!";

	Sleep(5000);

	return 0;
}

